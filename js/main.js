$(document).ready(function(){
    $(".img-box").hover(function(){
        let index =  $(this).index();
        let img =$('.img-box:eq(' + index + ') .bg-img').css('background-image');
        $('.left-container').toggleClass('active').fadeIn(200);
        $('.left-container.active').css('background-image',`${img}`).fadeIn(200);

        // Text
        
        let text = $('.img-box:eq(' + index + ') .text p').html();
        $('.left-container.active .list-text').html(text).fadeIn(200);


        let title = $('.img-box:eq(' + index + ') .title p').html();
        $('.left-container.active .heading-text').html(title).fadeIn(200);
    })
})

// $(document).ready(function(){
//     $(".img-box").hover(function(){
//         let index =  $(this).index();
//         let img =$('.img-box:eq(' + index + ') .bg-img').css('background-image');
//         $('.left-container').fadeToggle(function(){
//             $('.left-container').css('background-image',`${img}`).fadeIn();
//         })
//     })
// })